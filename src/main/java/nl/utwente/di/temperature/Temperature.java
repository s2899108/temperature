package nl.utwente.di.temperature;

import java.util.HashMap;

public class Temperature {

    public double getFahrenheit(String degrees) {
        double temp = Double.parseDouble(degrees);
        temp = (temp * 1.8) + 32;
        return temp;
    }
}
